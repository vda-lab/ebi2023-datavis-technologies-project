= Implementing data visualisations using Svelte: Project EBI 2023
:author: Jan Aerts
:email: jan.aerts@kuleuven.be
:url: http://vda-lab.github.io
:multipage-level: 0
:toc: left
:sectnums: true
:source-highlighter: coderay
:imagesdir: assets
:stem: latexmath
:stylesheet: jandot.css
:toclevels: 3
:favicon: ./assets/favicon.png
ifdef::backend-pdf[]
:title: Implementing data visualisations using Svelte: Project EBI 2023
:doctype: book
endif::[]

image:air_quality_cropped.jpeg[]

Prof Jan Aerts +
Visual Data Analysis Lab +
http://vda-lab.github.io +
jan.aerts@vda-lab.io | jan.aerts@kuleuven.be

Teaching assistant: Jelmer Bot (email: `firstname.lastname@uhasselt.be`)

This material is part of the Biological Data Visualisation course at the European Bioinformatics Institute, 2023. Worked out by Ryo Sakai, Jelmer Bot & Jan Aerts.

ifdef::backend-pdf[]
An online version is available at http://vda-lab.gitlab.io/ebi2023-datavis-technologies-project.
endif::[]

The material is available under a CC-BY-NC license. It can be copied/remixed/tweaked/..., but the original author needs to be credited. It can also not be used for commercial purposes except by the original author.

https://creativecommons.org/licenses/by/4.0/[image:ccbync.png[]] [.small]#(C) Jan Aerts, 2022-2023# image:vda-lab_logo_large.png[width=10%,pdfwidth=10%]

== Before doing this project
Jelmer has created a set of exercises which allow you to work on the atomic aspects of what you'll do in the project. Please perform these exercises _before_ you start working on the project. You can find the exercises at https://docs.google.com/spreadsheets/d/1gBiwB9YaAefbv7sbB-t7On_SKGOx5G5-tW-Rw4i9ZlE/edit?usp=sharing

== The general idea for the project
For this project, we will explore the post-translational modification dataset that was made available for the Bio/MedVis data visualisation challenge in 2022. Data is available for 3 proteins, both in human and in mouse (ergo: 6 proteins in total).

== The dataset
The data is available as a JSON file at https://vda-lab.github.io/assets/ebi_ptm.json and looks like this:

[source,json]
----
{
    "name": "P04202",
    "full_name": "Transforming growth factor beta-1 proprotein",
    "symbol": "TGFB1_MOUSE",
    "length": 390,
    "species": "mouse",
    "function": "Transforming growth factor beta-1 proprotein: Precursor of the Latency-associated peptide (LAP) and Transforming growth factor beta-1 (TGF-beta-1) chains, which constitute the regulatory and active subunit of TGF-beta-1, respectively.",
    "nr_modifications": 2821,
    "residues": [
        {
            "pos": 1,
            "residue": "M",
            "modifications": []
        },
        {
            "pos": 2,
            "residue": "P",
            "modifications": []
        },
        ...
        {
            "pos": 60,
            "residue": "A",
            "modifications": [
                {
                    "mod": "[127] Fluoro",
                    "type": "Chemical derivative",
                    "prob": 1
                },
                {
                    "mod": "[53] HNE",
                    "type": "Post-translational",
                    "prob": 0.25
                }
            ]
        },
        ...
        ]
}
----

For each protein, we have general information such as name, symbol and length, as well as a list of residues. For each residue, we have the position, the aminoacid at that position, and the list of modifications. For most residues the latter will be an empty list. For residues that _do_ have modifications, we have the name of the modification, the type and probability. To summarise:

* for each protein:
** name
** full_name
** symbol
** length
** species ("mouse" or "human")
** function
** number of modifications
** array of residues

* for each of those residues:
** position
** aminoacid
** list of modifications

* for each of those modification:
** name
** type
** probability

== The design
You can see VDA-lab's (Jelmer Bot, Jannes Peeters & Jan Aerts) submission to the competition here: 
https://biovis2022.vercel.app/. This is however _not_ the design that we will be focussing on this project.

Below follows a description of the design that you'll be implementing.

IMPORTANT: Feel free to adapt these designs. You can add functionality, change functionality, decide to skip something.

=== Overview page
An overview page should include a scatterplot showing each protein based on their length (`length`), and the number of modifications (`nr_modifications`) in that protein. The points should be coloured by species.

image::overview.png[width=75%]

Interaction on this plot:

* when _hovering_ over a point, the user should see some details for that protein (e.g. name, length and species)
* when _clicking_ on a point, the user should be redirected to a details page for that proten, described below

[NOTE,caption=SUGGESTIONS]
====
After you get this to work, extract the code for the scatterplot into its own component as described in https://vda-lab.gitlab.io/datavis-technologies/_custom_components_2.html. You might want to re-use it later (see next section).
====

=== Details page, version 1
The details page should list the general details of the protein, as well as 2 visualisations.

image::details_1.png[width=75%]

The **first visual** should be a scatterplot, matrix of heatmap showing position in the protein on the x-axis, and modification type on the y-axis.

The **second visual** should also show an x-axis representing the position, with a histogram showing the "real" modifications above it and a histogram showing the "artefact" modifications downwards.

NOTE: You are free to choose what binsize to use for the histogram; it's the principle that counts.

When _clicking_ somewhere on the axis, the picture should zoom in 10x around that position. A `Reset zoom` button should zoom out to show the complete protein again.

=== Details page, version 2
Next, you should create a _circular_ version of the histogram-based plot:

image::details_2.png[width=75%]

NOTE: When you have the circular version, you might want to use that instead of the circles in the overview page as well.
